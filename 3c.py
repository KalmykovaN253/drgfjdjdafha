def lucas(n):
    a1 = 2
    a2 = 1
    res = [2, 1]
    for i in range(n):
        a2, a1 = a1 + a2, a2
        res += [a2]
    return res[:-1]
    
print(lucas(int(input())))