import matplotlib.pyplot as plt

def fibonacci(n):
    a1 = 0
    a2 = 1
    for i in range(n):
        a2, a1 = a1 + a2, a2
    return(a1)
    
def rec_fibonacci(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    else:
        return(rec_fibonacci(n-1)+rec_fibonacci(n-2))
        
def to_even(a):
    b = []
    for elem in a:
        if int(elem) % 2 == 0:
            b += [int(elem)]
    return b

n = int(input())
print(fibonacci(n))
print(rec_fibonacci(n))
print(to_even(input().split()))